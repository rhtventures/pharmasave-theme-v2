<?php get_header(); ?>
 
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white">About</h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>
      
<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3">
            
            <div class="col-12">
               
                  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                 
                  <?php      
                    echo '<div class="row border-bottom pb-4 mb-4">';
                      echo '<div class="col-lg-5">';
                      echo '<div class="px-2">';
                          echo '<h4>'.get_field('location_name').'</h4>';
        
                          $image = get_field('location_image');
                          echo '<img class="ir mb-2" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';

                          echo '<p><strong>Address:</strong><br/>';
                          echo ''.get_field('location_address_street').'<br/>';
                          echo ''.get_field('location_address_city').',&nbsp;';
                          echo ''.get_field('location_address_province').'&nbsp;';
                          echo ''.get_field('location_address_postal').'<br/>';
                          echo '<strong>Phone: </strong>'.get_field('location_phone_number').'&nbsp; | &nbsp;';
                          echo '<strong>Fax: </strong>'.get_field('location_fax_number').'</p>';
                          echo '<strong>Store Hours:</strong>';
                          echo ''.get_field('location_hours').'';
                          echo '<a href="'.get_permalink().'">Store Details</a> | <a href="/contact">Contact Store</a>';
                  echo '</div>';
                  echo '</div>';

                  echo '<div class="col-lg-7 pt-5">';
        
                        echo '<div class="embed-responsive embed-responsive-21by9"><iframe width="625" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?ie=UTF8&amp;q='.get_field('location_address_street').','.get_field('location_address_province')[0].'+'.get_field('location_address_postal').'&amp;fb=1&amp;gl=ca&amp;output=embed"></iframe></div>';
                               
                  echo '</div>';
                echo '</div>';//row
        
               ?>
        
               <?php endwhile; endif; ?>
 
            </div>
        </div><!--/.row-->
    </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  

<?php 	get_footer(); ?>