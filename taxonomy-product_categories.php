<?php get_header(); ?>
<?php
$term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white">Products / <?php echo $term->name; ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3 products border-bottom">
          
           <?php if(have_posts()) : while (have_posts()) : the_post(); ?>   
          
              <?php              

               echo '<div class="col-6 col-md-4 col-lg-3">';

                        echo '<div class="product-item border relative mb-4">';
          
                            echo '<a href="'.get_permalink().'">';
                            echo '<div class="image-crop">';
                               $image = get_field('product_image');
                               echo '<img class="ir p-4" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                            echo '</div>';

                            echo '<h6 class="d-flex align-items-center justify-content-center border-top m-0 py-2 px-4 bg-dark">'.get_the_title().'</h6>';

                            echo '<div class="product-price">'.get_field('product_price').'</div>';
                            echo '</a>';
                  
                        echo '</div>'; 
                    echo '</div>'; 
              ?>
 
          <?php endwhile; endif; ?>
          
          <div class="col-12">           
             <?php echo ''.the_posts_pagination();?> 
          </div>
      </div>   
      
    </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  

<?php 	get_footer(); ?>