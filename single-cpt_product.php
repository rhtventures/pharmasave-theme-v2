<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
           <h1 class="white"><?php echo get_the_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
        
        <div class="row">
 
            <?php              
              echo '<div class="col-sm-4">';
                  echo '<div class="border p-4 bg-light">';
                      $image = get_field('product_image');
                      echo '<a href="'.$image['url'].'" title="'.$image['title'].'" data-toggle="lightbox" data-gallery="gallery" >'; 
          
                            echo '<img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                      echo '</a>';
                  echo '</div>';
              echo '</div>';
                
              echo '<div class="col-sm-8">';
                 
                    echo '<h4>'.get_the_title().'</h4>';
                    echo '<div class="bg-red white d-inline p-2">'.get_field('product_price').'</div><br/><br/>';
          
                    echo '<div class="py-2 border-top border-bottom mb-5">';
                      echo '<strong>Product Description:</strong><br/> '.get_field('product_description').'';
                    echo '</div>';

          
                    $term_list = wp_get_post_terms($post->ID, 'product_categories', array("fields" => "all"));       
          
                  if( count( $term_list ) ):
                    
                      foreach( $term_list as $term ){
                        echo '<p><a class="btn btn-small text-white" href="'.get_term_link( $term ).'"><i class="fas fa-arrow-left"></i> View more '.$term->name.' Products</a>';                         
                      }
                    
                  else: 
          
                  echo '<p><a class="btn btn-small text-white" href="/products"><i class="fas fa-arrow-left"></i> View more Products</a>';
                              
                  endif;
          
               echo '</div>';
             ?>

        </div>
         
       <?php endwhile; endif; ?>  
      
     </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 

<?php 	get_footer(); ?>

<script type="text/javascript">    
$(document).on("click", '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
</script>