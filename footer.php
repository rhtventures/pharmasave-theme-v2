<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

?>

<section class="mb-3 footer">
    <div class="container pb-4 mb-1 border-bottom">
        <div class="no-gutter row">
            
            <div class="col-lg-6 bg-red">
                
                <div class="p-4 p-lg-5 text-center">
                  <i class="fas fa-envelope"></i>
                  <h4 class="text-white my-1">Email Sign Up</h4>
                    <p class="text-white">Get all the specials, weekly sales, deals and offers from Canada's community pharmacy.</p>
                  
                    <div class="signup my-4">
                      <a href="/email-signup" class="white text-uppercase">Sign Up Today</a>
                     </div>
                 </div>
              
                
            </div><!--/Email Sign Up-->
             
            <div class="col-lg-6 bg-light"> 
              
              <div class="p-4 p-lg-5 text-center">
                
                    <?php if( get_option('pharmacy_facebook_url')!='' or get_option('pharmacy_twitter_url')!='' or get_option('pharmacy_instagram_url')!='' or get_option('pharmacy_linkedin_url')!='' or get_option('pharmacy_youtube_url')!='' ): ?>  
                      <i class="fas fa-comments text-muted"></i>
                      <h4 class="red my-1">Connect with Us</h4>
                      <p>Follow us for bright ideas and must-haves for every season.</p>

                      <div class="social pt-2">
                         <?php if( get_option('pharmacy_facebook_url') ): ?>
                            <a href="<?php echo get_option('pharmacy_facebook_url') ?>" target="_blank"><i class="fab fa-facebook"></i></a>
                        <?php endif; ?>

                        <?php if( get_option('pharmacy_twitter_url') ): ?>
                            <a href="<?php echo get_option('pharmacy_twitter_url') ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                        <?php endif; ?>

                        <?php if( get_option('pharmacy_instagram_url') ): ?>
                            <a href="<?php echo get_option('pharmacy_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                        <?php endif; ?>

                        <?php if( get_option('pharmacy_linkedin_url') ): ?>
                           <a href=" <?php echo get_option('pharmacy_linkedin_url') ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        <?php endif; ?>

                        <?php if( get_option('pharmacy_youtube_url') ): ?>
                            <a href="<?php echo get_option('pharmacy_youtube_url') ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                        <?php endif; ?> 
                    </div>
                
                      <?php else: ?>
                      <i class="fas fa-prescription-bottle-alt text-muted"></i>
                      <h4 class="red my-1">eRefills</h4>
                      <p>Order your prescription refills online, view your prescription history and see details about each medication.</p>

                      <div class="erefill my-4">
                        <a href="/rx-refill" class="text-uppercase">Order Your Prescription</a>
                      </div>
                <?php endif; ?>
                    
                </div>
            
              
             </div><!--/Social Media-->
            
        </div><!--/.row-->
    </div><!--/.container-->
    
    <div class="text-center"><p class="copy"><small>&copy; <?php echo date("Y");?>  Pharmasave.  All rights reserved. <a href="/privacy-policy">Privacy Policy</a></small></p></div>
     
</section>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/global.js"></script>

 <?php wp_footer(); ?>

</body>

</html>