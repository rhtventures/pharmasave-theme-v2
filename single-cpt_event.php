<?php get_header(); ?>
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
          <?php if(have_posts()) : while (have_posts()) : the_post(); ?> 
          
           <h1 class="white"><?php echo get_the_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3 border-bottom ">
                             
            <?php   
          
              $date_start = str_replace('/', '-', get_field('event_date_start') );
              $date_end = str_replace('/', '-', get_field('event_date_end') );          

               echo '<div class="col-12 text-center">';

                  echo '<div class="bg-light p-4 mb-3">';
                      $image = get_field('event_image');
                      echo '<img class="ir" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                  echo '</div>';

                  if( $date_start != $date_end )
                      echo '<h4 class="mb-1">'.date( 'M d, Y', strtotime( $date_start ) ).'&nbsp;-&nbsp; '.date( 'M d, Y', strtotime( $date_end ) ).'</h4>';
                  else
                      echo '<h4 class="mb-1">'.date( 'M d, Y', strtotime( $date_start ) );

                  echo '&nbsp; | &nbsp;'.get_field('event_location').'</h4>';

                echo '<div class="py-3">';
                echo ''.get_field('event_description').'';
                echo '</div>';


          
                echo '<div class="pt-3 border-top"><p>';
          
          
                    $term_list = wp_get_post_terms($post->ID, 'event_categories', array("fields" => "all"));       

                    if( count( $term_list ) ):

                        foreach( $term_list as $term ){
                          echo '<a class="btn btn-small text-white" href="'.get_term_link( $term ).'"><i class="fas fa-arrow-left"></i> View more '.$term->name.' Events</a>';
                        }

                    else: 

                    echo '<a  class="btn btn-small text-white" href="/events"><i class="fas fa-arrow-left"></i> View more Events</a>';

                    endif;           
          
                    
                echo '</div>';
            echo '</p></div>';
          ?>       

          <?php endwhile; endif; ?>
       
  
        </div><!--/.row-->
    </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 

<?php 	get_footer(); ?>


          