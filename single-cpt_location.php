<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
      
        <div class="text-center bg-red p-5">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
           <h1 class="white"><?php echo get_the_title(); ?></h1>
         </div>
      
         <?php get_template_part('/page-templates-parts/sub-nav'); ?> 
          
    </div><!--/.container-->
</section>
  
<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3">
            
            <div class="col-12 border-bottom">
                <?php      
                    echo '<div class="row border-bottom pb-4 mb-4">';
                      echo '<div class="col-md-7">';
                        echo '<div class="px-2">';
                              $image = get_field('location_image');
                              echo '<img class="ir mb-2" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';

                              echo '<p><strong>Address:</strong><br/>';
                              echo ''.get_field('location_address_street').'<br/>';
                              echo ''.get_field('location_address_city').',&nbsp;';
                              echo ''.get_field('location_address_province').'&nbsp;';
                              echo ''.get_field('location_address_postal').'<br/>';
                              echo '<strong>Phone: </strong>'.get_field('location_phone_number').'';
                 ?>

                              <?php if( get_field('location_fax_number') ): ?>
                                  &nbsp; | &nbsp;   <strong>Fax:</strong> <?php the_field('location_fax_number'); ?></a><br/><br/>
                              <?php endif; ?>

                  <?php    
                              echo '<strong>Store Hours:</strong>';
                              echo ''.get_field('location_hours').'';
          
                             
          
                        echo '</div>';
                      echo '</div>';
                      // Location Map //
                      echo '<div class="col-md-5" id="contact-form">';
          
                        echo '<div class="p-3 bg-light">';
          
                                    echo ''.get_field('location_contact_form').'';
                        echo '</div>';
                      echo '</div>';
              echo '</div>';//row
          
              echo '<div class="row">';
           echo '<div class="embed-responsive embed-responsive-21by9"><iframe width="625" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?ie=UTF8&amp;q='.get_field('location_address_street').','.get_field('location_address_province')[0].'+'.get_field('location_address_postal').'&amp;fb=1&amp;gl=ca&amp;output=embed"></iframe></div>';
          
          echo '</div>';
          ?>
          
        <?php
              echo '<div class="row pt-4">';
                  echo '<div class="col-12 mb-2">';
                      echo ''.get_field('location_description').'';
                  echo '</div>';
              echo '</div>';
             ?>
      </div>
      
      <div class="col-12 text-center mb-3 pt-3">

          <script src="https://cdn.jsdelivr.net/gh/stevenmonson/googleReviews@6e8f0d794393ec657dab69eb1421f3a60add23ef/google-places.js"></script>
          <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo get_field('location_googlemb_api');?>&signed_in=true&libraries=places"></script>

          <div class="reviews-container-home">
              <h3 class="red mb-4">What Our Customers Say</h3>
              <div id="google-reviews" class="mb-2"></div>
 
              <a href="https://search.google.com/local/writereview?placeid=<?php echo get_field('location_googlemb_location_id');?>" target="_blank">Leave a Review</a>
 

              <script>
              jQuery(document).ready(function( $ ) {
                 $("#google-reviews").googlePlaces({
                      placeId: '<?php echo get_field('location_googlemb_location_id');?>' 
                    , render: ['reviews']
                    , min_rating: 4
                    , max_rows:5
                 });
              });
              </script> 
            
          </div><!--/.reviews-container-home-->

        </div><!--/.row-->
      
      <?php endwhile; endif; ?>
      
           <div class="col-12">
              <?php echo '<p><a class="btn btn-small text-white" href="/locations"><i class="fas fa-arrow-left"></i> View more Locations</a>'; ?>
           </div>
      
        </div><!--/.row-->
    </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 

<?php 	get_footer(); ?>