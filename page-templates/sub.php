<?php /* Template Name: Sub Page */ ?>
<?php get_header(); ?>
 
<div class="page-space mt-lg-5 mb-lg-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
              
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <h1 class="white"><?php single_post_title(); ?></h1>
           
        </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>
      
<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3 border-bottom">
            
            <div class="col-12">
                
 
                <?php the_content(); ?>
                <?php endwhile; endif; ?>
      
             </div>
        </div><!--/.row-->
    </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 
 
<?php 	get_footer(); ?>