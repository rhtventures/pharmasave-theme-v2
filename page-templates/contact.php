<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <h1 class="white"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h1>
           
        </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>
      
<section class="py-5 page-content">
    <div class="container">
      <?php          
                  $loop = new WP_Query( array('post_type' => 'cpt_location','orderby'=>'menu_order','order'=>'ASC','posts_per_page' => 100) );             
                  while ( $loop->have_posts() ) : $loop->the_post();
              ?>
        <div class="row pb-3 border-bottom mb-4">
             
            <div class="col-lg-6">
 
              
              <?php
                    echo '<h4>'.get_field( 'location_name' ).'</h4>'; 

                    echo '<div class="row">';
                        echo '<div class="col-sm-6">';

                            echo '<strong>Address</strong><br/>';

                            echo get_field( 'location_address_street' ).'<br/>'; 
                            echo get_field( 'location_address_city' ).',&nbsp;'; 
                            echo get_field( 'location_address_province').'&nbsp;'; 
                            echo get_field( 'location_address_postal' ).'<br/><br/>'; 

                            echo  '<strong>Phone: </strong>'.get_field( 'location_phone_number' ).'<br/>'; 

                 ?>
                           <?php if( get_field('location_fax_number') ): ?>
                              <strong>Fax:</strong> <?php the_field('location_fax_number'); ?></a><br/><br/>
                           <?php endif; ?>
                     
                    <?php
                         echo '</div>'; 

                         echo '<div class="col-sm-6">';
                                echo '<strong>Store Hours</strong><br/>';
                                echo get_field( 'location_hours' ).'';
                              echo '</div>';
          
                          echo '</div>';  
          
                          if ( $loop->post_count == 1 ){
                               echo '<div class="embed-responsive embed-responsive-16by9"><iframe width="625" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?ie=UTF8&amp;q='.get_field('location_address_street').','.get_field('location_address_province')[0].'+'.get_field('location_address_postal').'&amp;fb=1&amp;gl=ca&amp;output=embed"></iframe></div>';
                          } else {
                               echo '<a class="btn btn-small" href="'.get_permalink().'#hours">Location Details</a>';
                          }

                    ?>

            </div><!--/left-column-->           
      
              <div class="col-lg-6 mb-3">
                    <?php
                      if ( $loop->post_count == 1 ){
                          echo '<div class="p-3  mb-4 bg-light">'.get_field('location_contact_form').'</div>'; 
                      } 
                      else {
                           echo '<div class="embed-responsive embed-responsive-16by9"><iframe width="625" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?ie=UTF8&amp;q='.get_field('location_address_street').','.get_field('location_address_province')[0].'+'.get_field('location_address_postal').'&amp;fb=1&amp;gl=ca&amp;output=embed"></iframe></div>';
                      }

         
                    ?>



            </div><!--/right-column-->
          
                
        </div><!--/.row-->
    <?php endwhile; ?>
          <?php endwhile; endif; ?>
    </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 

<?php 	get_footer(); ?>