<?php /* Template Name: Product Categories*/ ?>
<?php get_header(); ?>
 
<div class="page-space mt-lg-5 mb-lg-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
              
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <h1 class="white"><?php single_post_title(); ?></h1>
           
        </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>
      
<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3 border-bottom">
                 
                 <?php
                  $terms = get_terms( 'product_categories' );
                  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                      foreach ( $terms as $category ) {
                           $image = get_field('product_category_image', $category );
                        
                          echo '<div class="col-lg-3 text-center product-category-main">';
                          echo '<div class="p-3 bg-light mb-4">';
                            
                            echo '<a href="'. get_term_link( $category ) .'">';
                                echo '<div class="image-crop">';
                                  echo '<img class="ir mb-2" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                                echo '</div>';
                                echo '<h5>'.$category->name.'</h5>';
                            echo '</a>';
                        echo '</div>';
                        echo '</div>';
                      }
                  }          
                 ?>  
               
                <?php endwhile; endif; ?>
               
        </div><!--/.row-->
    </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 
 
<?php 	get_footer(); ?>