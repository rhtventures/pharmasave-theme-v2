<?php /* Template Name: Rx Refill */ ?>
<?php get_header(); ?>
 
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
            <h1><?php single_post_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
        <div class="header-image">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/rx-refills.jpg" class="img-fluid mb-2"/>
        </div>
      </div>
         
    </div><!--/.container-->
</section>

<section class="pb-5 page-content">
    <div class="container">
        <div class="row pb-5 border-bottom">
            
            <div class="col-12">
              <h1><?php single_post_title(); ?></h1>

              <?php 
              if (have_posts()) {
                while (have_posts()) {
                  the_post();
                  the_content(); 
                }
              } ?>    

              <div class="row justify-content-center">
                <?php          
                 $loop = new WP_Query( array('post_type' => 'cpt_location','orderby'=>'menu_order','order'=>'ASC','posts_per_page' => 100) );             
                 while ( $loop->have_posts() ) : $loop->the_post();
                ?>

                <div class="col-sm-6 col-lg-3 mb-3 text-center rx-refill-location">
                  
                    <?php                                          
                        //set logical URL  
                        $url_custom = get_field('location_rxrefill_url');
                        
                        $rx_url = ( $url_custom && $url_custom['url'] != '' )? $url_custom['url'] : 'http://www.pharmasave.com/prescriptions/?storeid='.get_field('location_store_id') ;                       
                        $rx_target = ( $url_custom && $url_custom['url'] != '' )? $url_custom['target'] : '_blank' ;                       
                    ?>
                  
                   <a class="red" target="<?php echo $rx_target; ?>" href="<?php echo $rx_url ?>">
                      <div class="border py-5">

                          <i class="fas fa-prescription-bottle-alt"></i>

                          <h4 class="mb-2 black"> <?php echo get_field('location_name') ?></h4>
                          Get Your Prescription
 
                      </div>
                     </a>
                </div>
    
              <?php endwhile; ?>
  
            </div>
        </div><!--/.row-->
          
     
    </div><!--/.container-->
</section>
 

<?php 	get_footer(); ?>