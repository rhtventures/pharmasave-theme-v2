<?php /* Template Name: Home Page */ ?>
<?php get_header();?>

<section class="banner">
  <div class="container pt-lg-5 mt-lg-5">
    <div id="homepage-banner" class="relative carousel slide carousel-fade" data-ride="carousel">
        <?php 
         $args = array(  
                  'post_type' => 'cpt_banner', 
                  'posts_per_page' => 100, 
                  'orderby' => 'menu_order',
                  //'meta_key'	=> 'banner_priority',
                  //'orderby'	=> 'meta_value',
                  'order'	=> 'ASC',
                  'tax_query' => array(
                    array(
                        'taxonomy' => 'banner_locations',
                        'field'    => 'slug',
                        'terms'    => 'home-page-top'
                    ),
                   )
         );

        $loop = new WP_Query($args); 

        ?>   

      <div class="carousel-inner">

         <?php 
            $slide_count = 0;
            while ( $loop->have_posts() ) : $loop->the_post();

                $image = get_field( 'banner_image' );  
                $link = get_field( 'banner_link' );

                echo '<div class="carousel-item '.(( $slide_count == 0 )? 'active' : '').'">';

                if( isset( $link['url'] ) )
                    echo '<a target="'.$link['target'].'" href="'.$link['url'].'" >';

                echo '<img src="'.$image['url'].'" class="img-fluid mb-3" alt="" title="'.get_the_title().'"/>';

                if( isset( $link['url'] ) )
                       echo '</a>';

                echo '</div>';

            $slide_count++;
            endwhile;
        ?>
         <div class="banner-nav">
             <a class="carousel-control-prev" href="#homepage-banner" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fas fa-arrow-left"></i></span>
            </a>
            <a class="carousel-control-next" href="#homepage-banner" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
            </a>
          </div><!--/.banner-nav-->
       
    </div><!--/.carousel-inner-->

  </div><!--/.homepage-banner-->
  </div>
</section>

<section class="py-4">
    <div class="container">
        <div class="row pb-3 border-bottom">
          
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          
            <div class="col-lg-6 mb-2">
                <div class="intro-title">

                    <div class="image-crop">
                      <?php 

                      $image = get_field('intro_image');

                      if( !empty($image) ): ?>

                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                      <?php endif; ?> 
                    </div>
                  
                <div class="intro-overlay text-center">
                    <h2 class="white">Live Well With Pharmasave</h2>
                </div>
            </div>
              
                <div class="intro-content">
                    <div class="intro-content-container">
                   
                      <?php the_content(); ?>
                       
                    </div>
                </div><!--/.intro-content-->
            </div>
            <?php endwhile; endif; ?>          
          
             <?php 
             $args = array(  
                        'post_type' => 'cpt_service', 
                        'posts_per_page' => 4,
                        'orderby'			=> 'menu_order',
                        'order'				=> 'ASC',
                       );

            $loop = new WP_Query($args); 

            ?>
          
            <div class="col-lg-6 home-services">
                <div class="row">
                  
                    <?php 
                    if (have_posts()) :  while ( $loop->have_posts() ) : $loop->the_post(); 
                        
                        $image = get_field('service_image');
                        $icon = get_field('service_icon');
                  
                    ?>
                    <div class="col-6 mb-4">
                        <a href="<?php echo get_permalink(); ?>">
                            <img src="<?php echo $image['url']; ?>" class="img-fluid relative"/>
                            <div class="overlay text-center">
                                <img src="<?php echo $icon['url']; ?>" class="mb-2"/>
                                <h3 class="white"><?php echo get_the_title(); ?></h3>
                            </div>
                        </a>
                    </div>
                    <?php endwhile; endif; ?>
                                      
                </div><!--/.row-->
            </div>

        </div><!--/.row-->
    </div><!--/.container-->
</section>
  
<?php 
  //get post info for first location
  $loop = new WP_Query( array('post_type' => 'cpt_event','meta_key'=> 'event_date_start','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 1, 'meta_query'=> 
            array(
              array(
                'key' => 'event_date_start',
                'value' => date('Y-m-d'),
                'compare' => '>=',
                'type' => 'DATE'
                )
              ) 
          ));   
  ?>
        
 <?php if( $loop->have_posts() ): ?>
  
      <section class="home-events text-center py-4 mb-5"> 
          <div class="container">
            
              <h3 class="red text-center mb-5">Upcoming Events</h3>
              <div class="row justify-content-center pb-5 border-bottom">

              <?php

                while ( $loop->have_posts() ) : $loop->the_post();


                $date_start = str_replace('/', '-', get_field('event_date_start') );
                $date_end = str_replace('/', '-', get_field('event_date_end') );                    
                
                  echo '<div class="col-lg-6">';
                
                      $image = get_field('event_image');
                        
                      echo '<a href="'.get_permalink().'"><img class="ir" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';
                       
                
                  echo '</div>';

                  echo '<div class="col-lg-6 text-left bg-light">';
                  echo '<div class="pt-4 px-2">';

                      echo '<div class="bg-secondary white mb-2 text-center d-inline-block" relative>';

                         if( $date_end != '' && $date_start != $date_end )
                           
                            echo '<h3 class="p-4 m-0">'.date( 'F d', strtotime( $date_start ) ).'&nbsp;-&nbsp; '.date( 'd, Y', strtotime( $date_end ) ).'</h3>';
                         else
                            echo '<h3 class="p-4 m-0">'.date( 'F d, Y', strtotime( $date_start ) ).'</h3>';
                
                          echo '</div>';


                          echo '<div class="pt-2 pb-0">';
                              echo '<a href="'.get_permalink().'" class="no-style"><h4 class="mb-1 text-dark">'.get_the_title().'</h4></a>';
                              echo wp_trim_words(get_field('event_description'), 20, '...<a class="red" href="'.get_permalink().'" class="no-style"> Read More</a>' );
                               
                          echo '</div>';
                
                          echo '<div class="home-event-link">';
                              echo '<a  class="btn btn-small text-white" href="/events"> View more Events <i class="fas fa-arrow-right"></i></a>';
                          echo '</div>';
                      echo '</div>';
                      echo '</div>';

                   endwhile;
                ?>
            </div><!--/.row-->
         </div><!--/.container-->
      </section><!--/.events-->
  
<?php endif;?> 
 
<?php 

   $args = array(  
            'post_type' => 'cpt_product', 
            'posts_per_page' => 4, 
            'orderby'			=> 'menu_order',
            'order'				=> 'ASC',
            'meta_query' => array(
                   array(
                       'key' => 'featured',
                       'value' => true,
                       'compare' => '='
                   )                 
             )
     );

    $loop = new WP_Query($args);

if( $loop->have_posts() ):
?>
  
<section class="py-4 home-products">
    <div class="container">
        
      <h3 class="red text-center mb-5">Featured Products</h3>
      
      <div class="row">
        
          <?php

              while ( $loop->have_posts() ) : $loop->the_post();

                $image = get_field( 'product_image' );  

                    echo '<div class="col-6 col-lg-3">';

                        echo '<div class="product-item border relative mb-4">';
        
                            echo '<a href="'.get_permalink().'">';
                            echo '<div class="image-crop">';
                               $image = get_field('product_image');
                               echo '<img class="ir p-4" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                            echo '</div>';

                            echo '<h6 class="d-flex align-items-center justify-content-center border-top m-0 py-2 px-4 bg-dark">'.get_the_title().'</h6>';

                            echo '<div class="product-price">'.get_field('product_price').'</div>';
                            echo '</a>';
                       echo '</div>'; 
                    echo '</div>'; 

              endwhile;
            ?> 
             
      </div><!--/.row-->
      <div class="col-12 text-center pt-3 pb-5 mb-4 border-bottom"> <a href="/products"> View All Products</a></div>          
      <?php endif; ?>
    </div><!--/.container-->
</section>
 
<section class="mb-4">

<?php 
  //get post info for first location
  $loop = new WP_Query( array('post_type' => 'cpt_location','orderby'=>'menu_order','order'=>'ASC','posts_per_page' => 1,
        'meta_query' => array(
         array(
             'key' => 'location_google_reviews',
             'value' => true,
             'compare' => '='
         ) )
  ));   
  ?>

 <?php if( $loop->have_posts() ): ?>        
  
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <div class="container">
        <div class="row text-center">

          <script src="https://cdn.jsdelivr.net/gh/stevenmonson/googleReviews@6e8f0d794393ec657dab69eb1421f3a60add23ef/google-places.js"></script>
          <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo get_field('location_googlemb_api');?>&signed_in=true&libraries=places"></script>

          <div class="reviews-container-home">
              <h3 class="red mb-4">What Our Customers Say</h3>
              <div id="google-reviews" class="mb-2"></div>
 
              <a href="https://search.google.com/local/writereview?placeid=<?php echo get_field('location_googlemb_location_id');?>" target="_blank">Leave a Review</a>
 

              <script>
              jQuery(document).ready(function( $ ) {
                 $("#google-reviews").googlePlaces({
                      placeId: '<?php echo get_field('location_googlemb_location_id');?>' 
                    , render: ['reviews']
                    , min_rating: 5
                    , max_rows:5
                 });
              });
              </script> 
            
            
          </div><!--/.reviews-container-home-->

        </div><!--/.row-->
    </div><!--/.container-->
    <?php endwhile; ?>
  
<?php endif; ?>  
  
</section>


<?php 	get_footer(); ?>
        
       
