<?php /* Template Name: Rx Refill (Omnisys) */ ?>
<?php get_header(); ?>
 
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
            <h1><?php single_post_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
      </div>
         
    </div><!--/.container-->
</section>

<section class="pb-5 page-content">
    <div class="container">
        <div class="row pb-5 border-bottom">
            
            <div class="col-12">
            
            <?php if( isset( $_GET['thankyou'] ) ): ?>
                <h4 class="mt-5"><?php single_post_title(); ?> Thank You</h4>
                <p>
                  Thank you!  Your order has been submitted to the Pharmasave team and will be ready for your pickup.  We look forward to seeing you soon.  
                  If you requested delivery, we will call you before we bring your prescription to tell you when the driver will be there and how much it will cost.
                </p>
            <?php else: ?>
                <form action="https://ca.centralrefill.com/partner_data_interface.php" id="refill" name="refill"  method="post" class="mt-5">
                    
                  <input type="hidden" name="Type_ID" value="1" />
                  <input type="hidden" value="<?php echo home_url(add_query_arg(array(), $wp->request)); ?>?thankyou=true" name="RedirectAfterSubmit" />

                  <h4 class="mt-5">Personal Information</h4>
              
                  <table style="width: 650px; background-color: #f3f2f2; padding: 10px;" border="0" cellspacing="0" cellpadding="2">
                      <tbody>
                          <tr>
                              <td style="width: 120px;" align="right"><strong>First Name:</strong></td>
                              <td style="padding-left: 10px;" align="left">
                                <input type="text" name="FirstName" size="30" maxlength="255" style="width:180px;" /> 
                                <input type="hidden" name="required" value="FirstName" /> 
                                <input type="hidden" name="FirstName.type" value="text" /> 
                                <input type="hidden" name="FirstName.message" value="You must enter a first name." /> *
                              </td>
                          </tr>
                          <tr>
                              <td align="right"><strong>Last Name:</strong></td>
                              <td style="padding-left: 10px;" align="left">
                                <input type="text" name="LastName" size="30" maxlength="255" style="width:180px;" /> 
                                <input type="hidden" name="required" value="LastName" /> 
                                <input type="hidden" name="LastName.type" value="text" /> 
                                <input type="hidden" name="LastName.message" value="You must enter a last name." /> *
                              </td>
                          </tr>
                          <tr>
                              <td style="background-color: #fafafa;">&nbsp;</td>
                              <td style="text-align: left; padding-left: 10px; background-color: #fafafa;">Last name must be entered exactly as it appears on the Prescription Label.</td>
                          </tr>
                          <tr>
                              <td style="text-align: right;"><strong>Phone Number:</strong></td>
                              <td style="padding-left: 10px;" align="left">
                                  ( <input onkeyup="moveTonext('CallBackAreaCode',3,'CallBackPrefix');" type="text" name="CallBackAreaCode" size="3" maxlength="3" style="width:60px;" /> ) 
                                  <input onkeyup="moveTonext('CallBackPrefix',3,'CallBackNumber');" type="text" name="CallBackPrefix" size="3" maxlength="3" style="width:60px;" /> - 
                                  <input onkeyup="moveTonext('CallBackNumber',4,'EmailAddress');" type="text" name="CallBackNumber" size="4" maxlength="4" style="width:80px;" /> *

                                  <input type="hidden" name="requiredNumber" value="CallBackAreaCode" /> 
                                  <input type="hidden" name="CallBackAreaCode.min" value="3" /> 
                                  <input type="hidden" name="CallBackAreaCode.max" value="3" /> 
                                  <input type="hidden" name="CallBackAreaCode.message" value="You must enter a 10 digit phone number. Letters are not valid, numbers only." /> 
                                  <input type="hidden" name="requiredNumber" value="CallBackPrefix" /> 
                                  <input type="hidden" name="CallBackPrefix.min" value="3" /> 
                                  <input type="hidden" name="CallBackPrefix.max" value="3" /> 
                                  <input type="hidden" name="CallBackPrefix.message" value="You must enter a 10 digit phonenumber. Letters are not valid, numbers only." /> 
                                  <input type="hidden" name="requiredNumber" value="CallBackNumber" /> 
                                  <input type="hidden" name="CallBackNumber.min" value="4" /> 
                                  <input type="hidden" name="CallBackNumber.max" value="4" /> 
                                  <input type="hidden" name="CallBackNumber.message" value="You must enter a 10 digit phonenumber. Letters are not valid, numbers only." />
                              </td>
                          </tr>
                          <tr>
                              <td style="background-color: #fafafa;">&nbsp;</td>
                              <td style="text-align: left; padding-left: 10px; background-color: #fafafa;">Number where you can be reached if the Pharmacist has a question.</td>
                          </tr>
                          <tr>
                              <td align="right"><strong>Email Address:</strong></td>
                              <td style="padding-left: 10px;" align="left">
                                <input type="text" name="EmailAddress" size="30" maxlength="255" style="width:220px;" /> 
                                <input type="hidden" name="isValidEmailorBlank" value="EmailAddress" /> 
                                <input type="hidden" name="EmailAddress.message" value="You must enter a valid email address." />
                              </td>
                          </tr>
                          <tr>
                              <td style="background-color: #fafafa;">&nbsp;</td>
                              <td style="text-align: left; padding-left: 10px; background-color: #fafafa;">Required only if you wish to receive an Email confiming your order was received by the Pharmacy. If you have not entered an Email address, please contact the pharmacy to confirm your prescription has been received.</td>
                          </tr>
                      </tbody>
                  </table>

                  <p>&nbsp;</p>

                  <h4 >Prescription Information</h4>

                  <p>Please enter the prescription number(s) you wish to refill at this time. This number is located on your prescription label (see example). All Prescriptions entered must match the Last Name as entered above.</p>

                  <table style="width: 650px; background-color: #f7f6f6; padding: 10px;" border="0" cellspacing="0" cellpadding="3">
                      <tbody>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                  <strong>Pharmacy Location</strong>
                                  <select name="Store_ID" id="rx-store-select">
                                      <?php  
                                       $loop = new WP_Query( array('post_type' => 'cpt_location',
                                                                   'orderby'=>'menu_order',
                                                                   'order'=>'ASC',
                                                                   'meta_key' => 'location_omnisys_storeid',
                                                                   'meta_value' => '',
                                                                   'meta_compare' => '!=',                                                                
                                                                   'posts_per_page' => 100) );  
                                                                       
                                       while ( $loop->have_posts() ) : $loop->the_post();
                                      ?>                                                                        
                                      <option value ="<?php echo get_field('location_omnisys_storeid'); ?>"><?php echo get_field('location_name') ?></option>
                                      <?php endwhile; ?>
                                  </select>
                              </td>
                              <td style="text-align: center; height:310px;width:310px;" rowspan="9">
                                  <div id="rx-image-label">
                                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/rx-label.gif" border="0" alt="" width="300" height="191" />
                                  </div>            
                              </td>
                          </tr>    
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #1:</strong> 
                                <input onkeyup="moveTonext('Rx1',8,'Rx2');" type="text" name="Rx1" size="7" maxlength="8" style="width:180px;" /> *
                                <input type="hidden" name="requiredNumber" value="Rx1" /> 
                                <input type="hidden" name="Rx1.message" value="The first prescription number block must be completed." /> 
                                <input type="hidden" name="Rx1.min" value="7" /> 
                                <input type="hidden" name="Rx1.max" value="8" />
                              </td> 
                          </tr>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #2:</strong> 
                                <input onkeyup="moveTonext('Rx2',8,'Rx3');" type="text" name="Rx2" size="7" maxlength="8" style="width:180px;" /> 
                                <input type="hidden" name="requiredNumberorBlank" value="Rx2" /> <input type="hidden" name="Rx2.message" value="Each prescription number entered must be numbers." /> <input type="hidden" name="Rx2.min" value="7" /> <input type="hidden" name="Rx2.max" value="8" />
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #3: </strong> 
                                <input onkeyup="moveTonext('Rx3',8,'Rx4');" type="text" name="Rx3" size="7" maxlength="8" style="width:180px;" /> 
                                <input type="hidden" name="requiredNumberorBlank" value="Rx3" /> <input type="hidden" name="Rx3.message" value="Each prescription number entered must be numbers." /> <input type="hidden" name="Rx3.min" value="7" /> <input type="hidden" name="Rx3.max" value="8" />
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #4: </strong> 
                                <input onkeyup="moveTonext('Rx4',8,'Rx5');" type="text" name="Rx4" size="7" maxlength="8" style="width:180px;" /> 
                                <input type="hidden" name="requiredNumberorBlank" value="Rx4" /> <input type="hidden" name="Rx4.message" value="Each prescription number entered must be numbers." /> <input type="hidden" name="Rx4.min" value="7" /> <input type="hidden" name="Rx4.max" value="8" />
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #5:</strong> 
                                <input onkeyup="moveTonext('Rx5',8,'Rx6');" type="text" name="Rx5" size="7" maxlength="8" style="width:180px;" /> 
                                <input type="hidden" name="requiredNumberorBlank" value="Rx5" /> <input type="hidden" name="Rx5.message" value="Each prescription number entered must be numbers." /> <input type="hidden" name="Rx5.min" value="7" /> <input type="hidden" name="Rx5.max" value="8" />
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #6: </strong> 
                                <input onkeyup="moveTonext('Rx6',8,'Rx7');" type="text" name="Rx6" size="7" maxlength="8" style="width:180px;" /> 
                                <input type="hidden" name="requiredNumberorBlank" value="Rx6" /> <input type="hidden" name="Rx6.message" value="Each prescription number entered must be numbers." /> <input type="hidden" name="Rx6.min" value="7" /> <input type="hidden" name="Rx6.max" value="8" />
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #7:</strong> 
                                <input onkeyup="moveTonext('Rx7',8,'Rx8');" type="text" name="Rx7" size="7" maxlength="8" style="width:180px;" /> 
                                <input type="hidden" name="requiredNumberorBlank" value="Rx7" /> <input type="hidden" name="Rx7.message" value="Each prescription number entered must be numbers." /> <input type="hidden" name="Rx7.min" value="7" /> <input type="hidden" name="Rx7.max" value="8" />
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: left; padding-left: 20px;">
                                <strong>Prescription #8:</strong> 
                                <input type="text" name="Rx8" size="7" maxlength="8" style="width:180px;" /> 
                                <input type="hidden" name="requiredNumberorBlank" value="Rx8" /> <input type="hidden" name="Rx8.message" value="Each prescription number entered must be numbers." /> <input type="hidden" name="Rx8.min" value="7" /> <input type="hidden" name="Rx8.max" value="8" />
                              </td>
                          </tr>
                      </tbody>
                  </table>

                  <table style="width: 650px; background-color: #f7f6f6; padding: 10px; margin: 10px 0px;" border="0" cellspacing="0" cellpadding="3">
                      <tbody>
                          <tr>
                              <td>&nbsp;</td>
                              <td>
                                  <p style="text-align: left;"><strong>* Would you like to:</strong></p>
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: right;"><input type="radio" value="Pickup" name="DeliveryMethod" /></td>
                              <td style="text-align: left;">Pickup your prescription</td>
                          </tr>
                          <tr>
                              <td style="text-align: right;"><input type="radio" value="Delivery" name="DeliveryMethod" /></td>
                              <td style="text-align: left;">Have your prescription delivered to you</td>
                          </tr>
                          <tr>
                              <td>&nbsp;</td>
                              <td>
                                  <p style="text-align: left;"><strong>Does your doctor need to be contacted to refill this prescription?</strong></p>
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align: right;"><input type="radio" value="ContactDoctor" name="NeedsAuthorization" /></td>
                              <td style="text-align: left;">Yes</td>
                          </tr>
                          <tr>
                              <td style="text-align: right;"><input type="radio" value="DontContactDoctor" name="NeedsAuthorization" /></td>
                              <td style="text-align: left;">No</td>
                          </tr>
                      </tbody>
                  </table>

                  <table style="width: 650px; background-color: #f7f6f6; padding: 10px; margin: 10px 0px;" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                          <td>
                              <input type="hidden" name="required" value="DeliveryMethod" /> 
                              <input type="hidden" name="DeliveryMethod.type" value="radio" /> 
                              <input type="hidden" name="DeliveryMethod.count" value="3" /> 
                              <input type="hidden" name="DeliveryMethod.message" value="You must indicate a method of receiving your order." /> 
                              <input id="submit" class="submit" onclick="return ValidateForm(1);" type="submit" name="submit" value="SUBMIT ORDER" />
                          </td>
                      </tr>
                  </table>
                  
                </form>
  
              <?php endif;?>
              
            </div>
        </div><!--/.row-->
          
     
    </div><!--/.container-->
</section>
 

<?php 	get_footer(); ?>