<?php /* Template Name: Email Signup */ ?>
<?php get_header(); ?>
 
 
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
            <h1><?php single_post_title(); ?></h1>
        </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
      
        <div class="header-image">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/email-signup.jpg" class="img-fluid mb-4"/>
        </div>
         
    </div><!--/.container-->
</section>

<section class="pb-5 page-content">
    <div class="container">
      
        <div class="col-12">
          <p class="text-center" style="font-size: 18px">To receive deals, valuable health information & more through email from Pharmasave, please choose your location. </p>
        </div>


       <div class="row justify-content-center border-bottom my-1 py-3">
        <?php          
         $loop = new WP_Query( array('post_type' => 'cpt_location','meta_key'=> 'location_priority','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 100) );             
         while ( $loop->have_posts() ) : $loop->the_post();
        ?>

        <div class="col-sm-6 col-lg-3 mb-3 text-center rx-refill-location">

          <a class="red" target="_blank" href="http://preferences.pharmasave.com/Preference.aspx?storeid=<?php the_field('location_store_id'); ?>">
              <div class="border py-5">

              <i class="fas fa-prescription-bottle-alt"></i>
              <h4 class="mb-2 black"><?php echo get_field('location_name') ?></h4>


              </div>
          </a>
      </div>

        <?php endwhile; ?>
    </div>
        </div><!--/.row-->
    </div><!--/.container-->
</section>
 

<?php 	get_footer(); ?>