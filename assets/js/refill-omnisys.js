// JavaScript Document		

function ValidateForm(iForm)
{
	//var strFormObject = eval('this.document.' + strFormName);
	//var objForm = strFormObject;
	var objForm = document.forms[iForm]; 
	var sField;
	
	for (i = 0; i < objForm.elements.length; i++) 
	{
		sField = objForm.elements[i].value;
	
		//alert("sField = " + sField);
					
		switch (objForm.elements[i].name)
		{
			case "required":
				if (!ValidateRequired(objForm, sField)) 
					return(false);				
				break;
			case "requiredNumber":
				if (!ValidateNumberOnly(objForm, sField)) 
					return(false);				
				break;
			case "requiredNumberorBlank":
				// blank okay so return fine	
				if (objForm[sField].value != "")
					if (!ValidateNumberOnly(objForm, sField)) 
						return(false);				
				break;
			case "matched":
				if (!ValidateMatched(objForm, sField)) 
					return(false);				
				break;
			case "isValidEmail":
				if (!ValidateEmail(objForm, sField)) 
					return(false);				
				break;
			case "isValidEmailorBlank":
				if (!ValidateEmailorBlank(objForm, sField)) 
					return(false);				
				break;								
			case "isValidPhone":
				if (!ValidateFixPhone(objForm, sField)) 
					return(false);				
				break;
			case "isValidPassword":
				if (!ValidatePassword(objForm, sField)) 
					return(false);				
				break;
			default:
				break;
		}
	}

	return(true);
}

function ValidateRequired(objForm, sField) {
	var sAlert;
	var sTypeIndex;
	var sFieldType;
	var sMessageIndex;
	var sMessage;

	sTypeIndex = sField + ".type";
	sFieldType = objForm[sTypeIndex].value;
	sMessageIndex = sField + ".message";
	sMessage = objForm[sMessageIndex].value;

	if (sFieldType == "text")
	{
		if (objForm[sField].value == "")
		{
			objForm[sField].focus();
			sAlert = sMessage;
			alert(sAlert);
			return(false);				
		}				
	}
	
	else if (sFieldType == "textarea")
	{
		if (objForm[sField].value == "")
		{
			objForm[sField].focus();
			sAlert = sMessage;
			alert(sAlert);
			return(false);				
		}				
	}

	else if	(sFieldType == "radio")
	{				
		var noneChecked = true;
		sMessageIndex = sField + ".count";
		var iCount = objForm[sMessageIndex].value;
		
		// check all the check boxes for this radio box
		for (var j = 0; j < iCount; j++)
		{
			//alert("j="+j+" => "+objForm[sField][j].checked);
			if (objForm[sField][j].checked == true)
			{
				// this check box has been selected
				noneChecked = false;
			}

		}		

		if (noneChecked)
		{			
		    objForm[sField][0].focus();
			sAlert = sMessage;
			alert(sAlert);
			return(false);
		}
	}
	
	else if	(sFieldType == "select")
	{
		var iIndex
		
		iIndex = objForm[sField].selectedIndex;
		
		if (iIndex < 0)
		{
			objForm[sField].focus();
			sAlert = sMessage;
			alert(sAlert);
			return(false);
		}
		
		if (objForm[sField].options[iIndex].value == "")
		{
			objForm[sField].focus();
	      	sAlert = sMessage;
			alert(sAlert);
			return(false);
		}	
	}
	
	else if	(sFieldType == "checkBox")
	{				
		var noneChecked = true;
		
		// check all the check boxes
		for (var j = 0; ; j++)
		{
			if (!objForm[sField][j])
			{
				break;
			}
			if (objForm[sField][j].checked == true)
			{
				// this check box has been selected
				noneChecked = false;
			}
		}		
					
		if (noneChecked)
		{
		    objForm[sField].focus();
			sAlert = sMessage;
			alert(sAlert);
			return(false);
		}
	}
	return(true);
}

function ValidateMatched(objForm, sField) {
	var sField1;
	var sField2Index;
	var sField2;
	var sMessageIndex;
	var sMessage;
	var sAlert;
	
	var sField1 = objForm[sField].value;
	var sField2Index = sField + ".mustmatch";
	sField2Index = objForm[sField2Index].value;
	var sField2 = objForm[sField2Index].value;
	sMessageIndex = sField + ".matchmessage";
	sMessage = objForm[sMessageIndex].value;
		
	if (sField1 != sField2)
	{
		var sAlert = sMessage;
		alert(sAlert);
		return(false);
	}
	return(true);
}

function ValidateEmail(objForm, sField) {
	var sAlert;
	var sField1;
	var sMessageIndex;
	var sMessage;

	sField1 = objForm[sField].value;
	sMessageIndex = sField + ".emailmessage";
	sMessage = objForm[sMessageIndex].value;

	var myRe, bRes;
	
	// constructs Regular Expression Object and sets pattern
	// to the value between the "/" characters
	myRe = /^([a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-])*)+@{1}([a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-])*)+\.{1}[a-zA-Z]{2,3}$/ig;

	// calls the test method of the Regular Expression Object
	// with string value parameter.
	//
	// returns boolean result
	if (!myRe.test(sField1))
	{
		objForm[sField].focus();
		sAlert = sMessage;
		alert(sAlert);
		return(false);
	}

	return(true);
}

function ValidatePassword(objForm, sField) {
	var sAlert;
	var sField1;
	var sMessageIndex;
	var sMessage;

	sField1 = objForm[sField].value;
	sMessageIndex = sField + ".pwmessage";
	sMessage = objForm[sMessageIndex].value;

	//do not validate empty password -- client should use a "required" check for that
	if (sField1 == "")
		return(true);
		
	// constructs Regular Expression Object and sets pattern
	// to the value between the "/" characters
	var myRe, bRes;

	myRe = /^[a-zA-Z0-9!,@#$]{5,20}$/ig;

	// calls the test method of the Regular Expression Object
	// with string value parameter.
	//
	// returns boolean result
	if (!myRe.test(sField1))
	{
		objForm[sField].focus();
		sAlert = sMessage;
		alert(sAlert);
		return(false);
	}

	return(true);
}

function ValidateEmailorBlank(objForm, sField) {
	var sAlert;
	var sField1;
	var sMessageIndex;
	var sMessage;

	sField1 = objForm[sField].value;
	// Allow blank to skip over !
	if (sField1 != "")
	{	
		sMessageIndex = sField + ".message";
		sMessage = objForm[sMessageIndex].value;
	
		var myRe, bRes;
		
		// constructs Regular Expression Object and sets pattern
		// to the value between the "/" characters
		myRe = /^([a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-])*)+@{1}([a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-])*)+\.{1}[a-zA-Z]{2,3}$/ig;
	
		// calls the test method of the Regular Expression Object
		// with string value parameter.
		//
		// returns boolean result
		if (!myRe.test(sField1))
		{
			objForm[sField].focus();
			sAlert = sMessage;
			alert(sAlert);
			return(false);
		}
	}
	return(true);
}

function ValidateNumberOnly(objForm, sField) {
	var sAlert;
	var sField1;
	var sMessageIndex;
	var sMessage;
	var iMinSize;
	var iMaxSize;

	sField1 = objForm[sField].value;
	sMessageIndex = sField + ".message";
	sMessage = objForm[sMessageIndex].value;
	sMessageIndex = sField + ".min";
	iMinSize = objForm[sMessageIndex].value;
	sMessageIndex = sField + ".max";
	iMaxSize = objForm[sMessageIndex].value;

	// constructs Regular Expression Object and sets pattern
	// to the value between the "/" characters
	var myRe, bRes;
	myRe = "/^[0-9!]{"+iMinSize+","+iMaxSize+"}$/ig";
	myRe = eval(myRe);  // to get rid of qoutes !!! 

	// calls the test method of the Regular Expression Object
	// with string value parameter.
	//
	// returns boolean result
	if (sField1 == "" || !myRe.test(sField1))
	{
		objForm[sField].focus();
		if (iMinSize == iMaxSize)
			sAlert = sMessage + "\n(" + iMinSize + " numbers in length for this field)";
		else
			sAlert = sMessage + "\n(" + iMinSize + " to " + iMaxSize + " numbers in length for this field)";

		alert(sAlert);
		return(false);
	}

	return(true);
}


function ValidateFixPhone(objForm, sField) {
	var sAlert;
	var sField1;
	var sMessageIndex;
	//var sMessage;

	sField1 = objForm[sField].value;
	sMessageIndex = sField + ".message";
	sAlert = objForm[sMessageIndex].value + "\nExample: 941 555 1234";

	// Toss them if nothing entered !!
	if (sField1 == "")
	{
		objForm[sField].focus();
		alert(sAlert);
		return(false);
	}
	// constructs Regular Expression Object and sets pattern
	// to the value between the "/" characters
	var myRe, bRes;

	myRe = /^[0-9!]{10,10}$/ig;

	// calls the test method of the Regular Expression Object
	// with string value parameter.
	//
	// returns boolean result
	//alert("test = " + myRe.test(sField1));

	if (!myRe.test(sField1))
	{
		var i;
		var sResult;
			
		  var a = sField1.split("");
		  var b = "";
		  for (var i in a)
		    if (a[i].match(/\d/))
		    b += a[i];
			
		  while (b.length>1 && b.charAt(0) == "0") b=b.substr(1,b.length);
	
		//alert ("b = *" + b + "*");  
		//alert ("b = " + b.length );  
		if (b == "" || b.length !=10)
		{
			if (b.length > 10)
				sAlert += "\n(You entered too many digits)";
			else
				sAlert += "\n(You did not entered enough digits)";				
			objForm[sField].focus();
			alert(sAlert);
			return(false);
		}
				
		//alert("test = " + myRe.test(b));
			
		if (!myRe.test(b))
		{
			objForm[sField].focus();
			alert(sAlert);
			return(false);
		}
	    objForm[sField].value = b;
	}

	return(true);
}



function moveTonext(sCurr,iLen,sNext)
{ 
 	var objForm = document.forms[0]; 
	if (objForm[sCurr].value.length == iLen)
	{
		objForm[sNext].focus();
	}	
}

//