// Main Menu Panel
// ------------------------------------------------------------------------------------------

        var revealPanel = function (buttonReveal, panel, buttonClose) {
          $(document).ready(function() {
            // Reveal panel 
            $(buttonReveal).on('click', function() {
              $(panel).addClass('expanded');
            });

            // Close panel
            $(buttonClose).on('click', function() {
              $(panel).removeClass('expanded');
            });   
           }); 
        }

        revealPanel('.reveal','.panel', '.panel-close');


// Search 
// ------------------------------------------------------------------------------------------
  $(document).ready(function(){
    $('a[href="#search"]').on('click', function(event) {                    
      $('#search').addClass('open');
      $('#search > form > input[type="search"]').focus();
    });            
    $('#search, #search .close').on('click keyup', function(event) {
      if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
        $(this).removeClass('open');
      }
    });            
  });



