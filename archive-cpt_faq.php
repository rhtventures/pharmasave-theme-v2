<?php get_header(); ?>
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white">About <!--<?php post_type_archive_title(); ?>--></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
           
          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>               
               
              <?php              
                echo '<div class="row">';
                    echo '<div class="col-12 pb-2">';
                        echo '<div class="faq-q">';
                            echo ''.get_field('faq_question').'';
                        echo '</div>';
                        
                        echo '<div class="bg-light pt-4 p-4">';
                              echo ''.get_field('faq_answer').'';
                        echo '</div>';
                    echo '</div>';
                   
                echo '</div>';
       echo '<hr/>';
              ?>
             
           <?php endwhile;?>
          </div><!--/.row-->
         
         <div class="row">
          <div class="col-12">
 
              <?php else : ?>
                There are currently no news items.
              <?php endif; ?>
          </div>
          <div class="col-12 pt-2">

            <?php echo ''.the_posts_pagination();?>
          </div>
  
     </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  

<?php 	get_footer(); ?>