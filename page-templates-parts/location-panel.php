<div class="row sm-gutter location-panel justify-content-md-center">
     <?php   
    $count_posts = wp_count_posts( 'cpt_location' );
  
    /*SINGLE LOCATION*/
    if( $count_posts->publish == 1 ):  
    ?>
  
        <?php 
          //loop through single locations
          $loop = new WP_Query( array('post_type' => 'cpt_location','orderby'=>'menu_order','order'=>'ASC','posts_per_page' => 100) );
          while ( $loop->have_posts() ) : $loop->the_post();
        ?>
  
            <div class="col-lg-6 location border-left">
                <div class="row pt-5 px-4">

                    <div class="col-sm-6">
                        <h5 class="white text-uppercase px-2 py-1 mb-1"><?php the_field('location_name'); ?></h5>  
                        <div class="px-1">
                      
                          <?php the_field('location_address_street'); ?> <br/>
                          <?php the_field('location_address_city'); ?>, <?php the_field('location_address_province'); ?><br/>
                          <?php the_field ('location_address_postal');?> <br/>
                          
                           <strong>P:</strong> <a href="tel:<?php the_field('location_phone_number'); ?>" class="no-style"><?php the_field('location_phone_number'); ?></a><br/>

                          <?php if( get_field('location_fax_number') ): ?>
                          <strong>F:</strong> <a href="tel:<?php the_field('location_fax_number'); ?>" class="no-style"><?php the_field('location_fax_number'); ?></a>
                            <?php endif; ?>
                      </div>
                     </div>

                    <div class="col-sm-6 store-hours">
                         <?php
                           echo '<h5 class="white text-uppercase px-2 py-1 mb-1">Store Hours:</h5>';
                           echo '<div class="px-2">'.get_field('location_hours').'</div>';
                          ?>
                    </div><!--/.store-hours-->
                   <div class="pt-4 px-3"> 
                    <p<small><a class="text-white bg-red p-2 font11" href="<?php echo get_permalink(); ?>">See more details</a></small> &nbsp; <a href="/contact" class="text-white bg-red p-2 font11">Contact Us</a></p>
                  </div>
                   

                </div><!--/.row-->
             </div><!--/location-->

            <div class="col-lg-6">
               <?php
                    echo '<div class="embed-responsive embed-responsive-21by9"><iframe width="625" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?ie=UTF8&amp;q='.get_field('location_address_street').','.get_field('location_address_province')[0].'+'.get_field('location_address_postal').'&amp;fb=1&amp;gl=ca&amp;output=embed"></iframe></div>';

                ?>
            </div><!--/Map-->
  
   
        <?php endwhile;?>
  
    <?php 
    /*MULTI LOCATION*/
    else: 
    ?>
      
        <?php 
          //loop through locations
          $loop = new WP_Query( array('post_type' => 'cpt_location','orderby'=>'menu_order','order'=>'ASC','posts_per_page' => 100) );
          while ( $loop->have_posts() ) : $loop->the_post();
        ?>
                  
            <div class="col-sm-6 col-lg-3 border-left ">
                <div class="p-4 location">
                
                  <h5 class="white text-uppercase px-2 py-1 mb-1"> <a href="<?php echo get_permalink(); ?>"><?php the_field('location_name'); ?></a> </h5> 
                    <div class="px-2">
                      
                        <?php the_field('location_address_street'); ?> <br/>
                        <?php the_field('location_address_city'); ?>, <?php the_field('location_address_province'); ?> <br/>
                        <?php the_field ('location_address_postal');?> <br/>

                        <strong>P:</strong> <a href="tel:<?php the_field('location_phone_number'); ?>" class="no-style"><?php the_field('location_phone_number'); ?></a><br/>

                        <?php if( get_field('location_fax_number') ): ?>
                        <strong>F:</strong> <a href="tel:<?php the_field('location_fax_number'); ?>" class="no-style"><?php the_field('location_fax_number'); ?></a>
                        <?php endif; ?>
                   </div>

                   
                    <div class="store-hours pt-4">
                         <?php
                            echo '<h5 class="white text-uppercase px-2 py-1 mb-1">Store Hours:</h5>';
                            echo '<div class="px-2">'.get_field('location_hours').'</div>';
                         ?>
                    </div><!--/.store-hours-->
                  <div class="pt-4"> 
                    <p><small><a class="text-capitalize text-white bg-red p-2 font11" href="<?php echo get_permalink(); ?>">See more details</a></small> &nbsp; <a href="/contact" class="text-capitalize text-white bg-red p-2 font11">Contact Us</a></p>
                  </div>
              </div>
            </div>
 
        <?php endwhile;?>
  
    <?php endif;?>  
  
</div>