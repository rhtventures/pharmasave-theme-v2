 <div class="subnav-bar">
    <?php if( is_single() ): ?>

        <?php 

            $_wp_query = $wp_query;

            //Replace with query for the page you want to masquerade as
            $wp_query=new WP_Query( array( 'post_type'=>get_post_type() ) );

            wp_nav_menu(
              array(
                'depth'             =>1,
                'sub_menu'			=> true,
                'container'         => '',
                'container_id'      => '',
                'container_class'   => '',
                'menu_class' 		=> 'list-unstyled sub-menu',
                'menu_id'			=> 'sub-menu',
              )
            );

            $wp_query = $_wp_query;

        ?>

        <?php else: ?>

        <?php 

          wp_nav_menu(
              array(
                'depth'             =>1,
                'sub_menu'			=> true,
                'container'         => '',
                'container_id'      => '',
                'container_class'   => '',
                'menu_class' 		=> 'list-unstyled sub-menu',
                //'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
                'menu_id'			=> 'sub-menu',
              )
          );  

        ?>  

     <?php endif; ?>

</div><!--/.subnav-bar-->