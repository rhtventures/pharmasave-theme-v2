<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white"><?php post_type_archive_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">

           <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
          
              <?php        
      
                    echo ' <div class="row pb-3 pl-5 mb-3 border-bottom"> ';
      
                       // echo '<div class="col-md-3">';
      
                             // $image = get_field('testimonial_image');
                             // echo '<a href="'.get_permalink().'"><img src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'" class="img-fluid"/></a>';
      
                       // echo '</div>';

                        echo '<div class="col-12">';
      
                              echo '<div class="quote">';
                                  echo '<i class="fas fa-quote-left"></i>';
                                  echo ''.get_field('testimonial_quote').'';
                              echo '</div>';
                
 
                              echo '<p><strong>'.get_field('testimonial_fname'). '&nbsp;' .get_field('testimonial_lname'). '</strong> | ';
                              echo '<strong>'.get_field('testimonial_title').'</strong></p>';

                        echo '</div>';
                    echo '</div>';
               ?>
              
          <?php endwhile;?>
     
         <div class="row">
            <div class="col-12">
               <?php else: ?>
                There are currently no testimonials.
              <?php endif; ?>
            </div>
           <div class="col-12 pt-2">
             <?php echo ''.the_posts_pagination();?>
         </div>
      </div>
    </div><!--/.container-->
</section>

<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  

<?php 	get_footer(); ?>