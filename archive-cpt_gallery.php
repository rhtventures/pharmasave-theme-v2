<?php get_header(); ?>


<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white"><?php post_type_archive_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
       
         <div class="px-3">
           
            <?php 
            $terms = get_terms('gallery_categories');
            if ( $terms ):
            ?>
              <div class="row justify-content-end"> 
                <div class="col-md-3 text-right">
                  <select name="category" id="selectcategory" class="form-control" onchange="javascript:location.href = this.value;">
                    <option value="/galleries">All Categories</option>
                    <?php
                      foreach ( $terms as $category ) {?>
                          <option value="/gallery-categories/<?php echo esc_attr( $category->slug )?>"><?php echo esc_html( $category->name ) ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            <?php endif; ?>    
      
            <div class="row no-gutter-sm pb-3 border-bottom gallery-list"> 
         
                  <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  

                       <?php              

                          echo '<div class="col-6 col-md-4 mb-1">';
                
                              echo '<div class="category-thumbnails">';
                                  echo '<div class="image-crop">';

                                       echo '<div class="cat-image">';
                                          $image = get_field('gallery_image');
                                          echo '<a href="'.get_permalink().'"><img class="ir" src="'.$image['url'].'" alt="'.$image['caption'].'" title="'.$image['title'].'"/></a>';
                                       echo '</div>';
                                  echo '</div>';

                                  echo '<div class="cat-text">';
                                      echo '<div class="cat-text-wrapper">';
                                          echo '<h4>'.get_the_title().'</h4>';
                                      echo '</div>';
                                       
                                  echo '</div>';
                                  echo '<div class="text-center"><a class="cat-link" href="'.get_permalink().'">'.get_the_title().'</a></div>';
              
                              echo '</div>';
                          echo '</div>';
                        ?>

                  <?php endwhile;?>

                <?php else : ?>
                  There are currently no galleries.
                <?php endif; ?>
           </div><!--/.row/event-blocks-->
           
          </div><!--/.px3-->
          
          <div class="col-12">           
             <?php echo ''.the_posts_pagination();?> 
          </div>
             
     </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  
 

<?php 	get_footer(); ?>