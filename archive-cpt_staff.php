<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white"> <?php post_type_archive_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>
    
<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3 border-bottom">
                 
                <?php if(have_posts()) : while (have_posts()) : the_post();?>                  
       
                    <?php      
                       
                        echo'<div class="col-6 col-lg-3 mb-3 staff-member text-center">';
                          echo '<div class="image-crop">';

                              $image = get_field('staff_image');
                              echo '<a href="#staff-details-'.get_the_ID().'" data-toggle="modal"><img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';
                          echo '</div>';
          
                              echo '<h5>'.get_field('staff_fname'). '&nbsp;' .get_field('staff_lname'). '</h5>';
                              echo '<p>'.get_field('staff_title').'</p>';  
 
                         echo '</div>';

                    ?>
              
                  <!----------------- Modal ----------------->
                  <div class="modal fade staff-modal" id="staff-details-<?php echo get_the_ID(); ?>" aria-hidden="true">
                      <div class="modal-dialog">
                          <div class="modal-body">
                              <a data-dismiss="modal"><i class="fas fa-times-circle"></i></a>
                                
                              <?php 
                                echo '<div class="text-center my-1">';
                                    $image = get_field('staff_image');
                                    echo '<img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                               
                                  echo '<h4 class="red mb-0">'.get_field('staff_fname'). '&nbsp;' .get_field('staff_lname'). '</h4>';
                                  echo '<p>'.get_field('staff_title').'</p>';  
                                echo '</div>';
                                echo ''.get_field('staff_description').''; 
                              ?>

                           </div><!--/.modal-body-->
                      </div><!--/.modal-dialog-->
                  </div><!--/.modal-->
              
                 <?php endwhile; endif; ?>
                
 
         </div><!--/.row-->
        <div class="row">
           <div class="col-12 py-2">
             <?php echo ''.the_posts_pagination();?>
           </div>
         </div>
     </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 


<?php 	get_footer(); ?>