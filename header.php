<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">

  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.ico" />
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <!-- CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/bootstrap-grid.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/main.css" rel="stylesheet" />
  
  <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet"> 
  <?php wp_head(); ?>

  <!-- JS -->  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/global.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/refill-omnisys.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
   
</head>

<body <?php body_class(); ?>>

 <div class="wrapper hfeed site" id="page">
    
      <header>
          <div class="container">

              <div class="row pt-lg-3">

                  <div class="col-lg-7 order-md-12 order-lg-1 logo">
                    <a href="/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="Pharmasave" title="Pharmasave" class="mb-3"/> </a> <h2> <?php echo get_option( 'pharmacy_store_name_full' );?></h2>
                  </div>

                  <div class="col-lg-5 order-md-1 order-lg-12 top-options">
                      <div class="row text-left text-md-center">

                          <div class="d-inline-block">
                               <a data-toggle="collapse" href="#locationPanel" role="button" aria-expanded="false" aria-controls="collapseExample">
                                  <?php   
                                    $count_posts = wp_count_posts( 'cpt_location' );
                                    /*SINGLE LOCATION*/
                                    if( $count_posts->publish == 1 ):  
                                  ?> 
                                      <i class="fas fa-store"></i><br/><span>Store Details</span></a> 
                                  <?php 
                                    /*MULTI LOCATION*/
                                    else: 
                                  ?>
                                      <i class="fas fa-store"></i><br/><span>Locations</span>

                                  <?php endif;?>  
                              </a>
                          </div>
                          <div class="d-inline-block">
                              <a href="/rx-refill"><i class="fas fa-prescription-bottle-alt"></i> <br/><span>Rx-Refills</span></a>
                          </div>
                          <div class="d-inline-block">
                               <a href="/flyer"><i class="fas fa-newspaper"></i> <br/><span>Store Flyer</span></a>
                          </div>
                          <div class="d-inline-block">
                              <a href="#search"><i class="fas fa-search"></i> <br/><span>Search</span></a>
                            
                                <!-- Search Form -->
                                  <div id="search"> 
                                    <span class="close"><i class="ion ion-close-circled white"></i></span>
                                      <form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
                                        
                                         <input value="" name="s" id="s" type="text" class="form-control" placeholder="What Are You Looking For?">
                                        <input id="searchsubmit" value="Search" type="submit" type="hidden">
                                        
                                    </form>
                                  </div> 
                            
                          </div>
                          <div class="site-menu">
                              <a href="#" class="reveal"><i class="fas fa-bars"></i><br/><span>MENU</span></a>
                          </div>

                      </div><!--/.row-->
                  </div>

              </div><!--/.row-->
            
               <div class="collapse" id="locationPanel">
                  <div class="card card-body">
                  
                      <?php get_template_part('/page-templates-parts/location-panel'); ?> 
                    
                      <script>
                           $(document).on('click',function(){
                            $('.collapse').collapse('hide');
                          })
                      </script>
                  
                  </div><!--/.card-->
              </div><!--/.collapse-->
        
          </div><!--/.container-->

          <div class="panel">
              <div class="panel-content">
                   <div class="panel-top">
                      <a href="index.php"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/panel-home.png"/></a>
                  <div class="panel-close"><span>Close</span> <i class="far fa-times-circle"></i></div>
               </div><!--/.panel-content-->

              <div class="pt-3 mt-3">
                    <?php wp_nav_menu(
                      array(
                        'depth'             => 2,//sub menu - increase to 2
                        'container'         => '',
                        'container_id'      => '',
                        'container_class'   => '',
                        'menu_class' 		=> 'list-unstyled panel-content mb-0',
                        'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
                        'menu_id'			=> 'main-menu',
                        )
                      ); 
                   ?>
              </div>

              <div class="social">
                
                  <a href="/contact"><i class="fas fa-map-marker-alt"></i></a>
                  <a href="/contact"><i class="fas fa-clock"></i></a> &nbsp;
                  
                  <?php if( get_option('pharmacy_facebook_url') ): ?>
                  <a href="<?php echo get_option('pharmacy_facebook_url') ?>" target="_blank"><i class="fab fa-facebook"></i></a>
                  <?php endif; ?>

                  <?php if( get_option('pharmacy_twitter_url') ): ?>
                  <a href="<?php echo get_option('pharmacy_twitter_url') ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                  <?php endif; ?>

                  <?php if( get_option('pharmacy_instagram_url') ): ?>
                  <a href="<?php echo get_option('pharmacy_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                  <?php endif; ?>

                  <?php if( get_option('pharmacy_linkedin_url') ): ?>
                  <a href=" <?php echo get_option('pharmacy_linkedin_url') ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                  <?php endif; ?>

                  <?php if( get_option('pharmacy_youtube_url') ): ?>
                  <a href="<?php echo get_option('pharmacy_youtube_url') ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                  <?php endif; ?>

                </div><!--/.social-->
            </div>
          </div><!--/.panel-->

          </div><!--/.container-->
      </header>
     
    