<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
      
        <div class="text-center bg-red p-5">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
           <h1 class="white"><?php echo get_the_title(); ?></h1>
         </div>
      
         <?php get_template_part('/page-templates-parts/sub-nav'); ?> 
          
    </div><!--/.container-->
</section>
  
<section class="py-5 page-content">
    <div class="container">
           
       
          <?php  
            echo '<div class="row">';
              echo '<div class="col-12">';
                echo ''.get_field('gallery_summary').'<br/><br/>';
              echo '<div>';
            echo '</div>';

            $photos = get_field('gallery_photos');

                  echo '<div class="row no-gutter border-bottom p-3">';
                      foreach( $photos as $photo ):
                          //print_r( $photo ); 
      
                            echo '<div class="thumbnail-1 wow fadeInLeft">';
                                echo '<div class="image-crop mb-0 border">';
                                     echo '<a href="'.$photo['url'].'" title="'.$photo['title'].'" data-toggle="lightbox" data-gallery="gallery" >'; 

                                      echo '<img class="ir" src="'.$photo['url'].'" alt="'.$photo['title'].'" title="'.$photo['title'].'"/>';

                                  echo '</a>';
                                echo '</div>';
      
                                echo '<div class="caption">';
                                  echo '<h5 class="white wow fadeInUp">'.$photo['title'].'</h5>';
                                echo '</div>  ';
                        echo '</div>'; 
                       endforeach; 

                   echo '</div>';
             
          ?>
     
      
       <?php endwhile; endif; ?>
      
           <div class="col-12">
              <div class="pt-4">
              <?php
              $term_list = wp_get_post_terms($post->ID, 'gallery_categories', array("fields" => "all"));       

              if( count( $term_list ) ):

                  foreach( $term_list as $term ){
                    echo '<p><a class="btn btn-small text-white" href="'.get_term_link( $term ).'"><i class="fas fa-arrow-left"></i> View more '.$term->name.' Galleries</a>';                         
                  }

              else: 

              echo '<a class="btn btn-small text-white"  href="/galleries"><i class="fas fa-arrow-left"></i> View more Galleries</a>';

              endif;

              ?>
             </div>
         </div>
      
        </div><!--/.row-->
    </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 

<?php 	get_footer(); ?>

<script type="text/javascript">    
$(document).on("click", '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
</script>
 