<?php get_header(); ?>
<?php
$term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white">News / <?php echo $term->name; ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
      
      <?php 
      $terms = get_terms('news_categories');
      if ( $terms ):
      ?>
        <div class="row justify-content-end"> 
          <div class="col-md-3 text-right">
            <select name="category" id="selectcategory" class="form-control" onchange="javascript:location.href = this.value;">
              <option value="/news">All Categories</option>
              <?php
                foreach ( $terms as $category ) {?>
                    <option <?php if( $term->slug == $category->slug ){ echo 'selected="selected"';} ?> value="/news-categories/<?php echo esc_attr( $category->slug )?>"><?php echo esc_html( $category->name ) ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      <?php endif; ?>    

      <div class="row pb-3 border-bottom"> 
      
           <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  

                 <?php              
                  echo '<div class="col-sm-6 col-md-4 col-lg-3 mb-4">';

                     echo '<div class="card article rounded-0">';
                          echo '<div class="image-crop">';
                          $image = get_field('news_image');
                          echo '<a href="'.get_permalink().'"><img src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';
                          echo '</div>';

                       echo '<div class="card-body">';
                            echo '<span class="date">'.get_the_date('F j, Y').'</span>';

                            echo '<h3 class="mt-0">'.get_the_title().'</h3>';
                            echo wp_trim_words(get_field('news_article'), 24, '... <a href="'.get_permalink().'"> read more</a>' );
                        echo '</div>';//card-body

                    echo '</div>';
                  echo '</div>';

                 ?>
             <?php endwhile;?>
         </div><!--/.row-->
      
        <div class="row">
          <div class="col-12">
 
              <?php else : ?>
                There are currently no news items.
              <?php endif; ?>
          </div>
           <div class="col-12 pt-2">

            <?php echo ''.the_posts_pagination();?>
         </div>
      </div>
             

     
    </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  

<?php 	get_footer(); ?>

