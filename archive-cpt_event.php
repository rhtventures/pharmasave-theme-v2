<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white"><?php post_type_archive_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>


<section class="py-5 page-content">
    <div class="container">
       
         <div class="px-3">      
            
            <?php 
            $terms = get_terms('event_categories');
            if ( $terms ):
            ?>
              <div class="row justify-content-end"> 
                <div class="col-md-3 text-right">
                  <select name="category" id="selectcategory" class="form-control" onchange="javascript:location.href = this.value;">
                    <option value="/events">All Categories</option>
                    <?php
                      foreach ( $terms as $category ) {?>
                          <option value="/event-categories/<?php echo esc_attr( $category->slug )?>"><?php echo esc_html( $category->name ) ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            <?php endif; ?>            
           
            <div class="row no-gutter-sm pb-3 border-bottom event-blocks"> 
          
                <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  

                 <?php    

                    $date_start = str_replace('/', '-', get_field('event_date_start') );
                    $date_end = str_replace('/', '-', get_field('event_date_end') );
              
                    echo '<div class="col-sm-6 col-lg-3 event-block"">';

                    echo '<a href="'.get_permalink().'">';
                    echo '<div class="p-4">';

                         echo '<div class="date white mb-3">';

                          if( $date_start != '' && $date_start != $date_end )
                              echo '<h1>'.date( 'd', strtotime( $date_start ) ).'-'.date( 'd', strtotime( $date_end ) ).'</h1>';

                          else                              
                              echo '<h1>'.date( 'd', strtotime( $date_start ) ).'</h1>';
                              echo date( 'F', strtotime( $date_start ) );

                          echo '</div>';

                          echo '<div class="event-summary">';

                              echo '<h4 class="mt-0 mb-1 white">'.get_the_title().'</h4>';

                              echo wp_trim_words(get_field('event_description'), 15, '...' );

                          echo '</div>';

                          echo ''.get_field('event_location').'';

                          echo '<i class="fas fa-external-link-alt"></i>';

                    echo '</div>';
                    echo '</a>';
                    echo '</div>';
                 ?>

                <?php endwhile;?>

                <?php else : ?>
                  There are currently no events.
                <?php endif; ?>
           </div><!--/.row/event-blocks-->
           
          </div><!--/.px3-->
          
          <div class="col-12">           
             <?php echo ''.the_posts_pagination();?> 
          </div>
             
     </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  




<?php 	get_footer(); ?>

