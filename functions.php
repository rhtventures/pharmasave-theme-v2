<?php

register_nav_menus( array( 
  'header' => 'Header'
) );

add_theme_support( 'title-tag' );


require get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';

add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );


// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  if ( isset( $args->sub_menu ) ) {
    $root_id = 0;

    // find the current menu item
    foreach ( $sorted_menu_items as $menu_item ) {
      if ( $menu_item->current ) {
        // set the root id based on whether the current menu item has a parent or not
        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
        break;
      }
    }

    // find the top level parent
    if ( ! isset( $args->direct_parent ) ) {
      $prev_root_id = $root_id;
      while ( $prev_root_id != 0 ) {
        foreach ( $sorted_menu_items as $menu_item ) {
          if ( $menu_item->ID == $prev_root_id ) {
            $prev_root_id = $menu_item->menu_item_parent;
            // don't set the root_id to 0 if we've reached the top of the menu
            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
            break;
          } 
        }
      }
    }
    $menu_item_parents = array();
    foreach ( $sorted_menu_items as $key => $item ) {
      // init menu_item_parents
      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;
      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
        // part of sub-tree: keep!
        $menu_item_parents[] = $item->ID;
      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
        // not part of sub-tree: away with it!
        unset( $sorted_menu_items[$key] );
      }
    }

    return $sorted_menu_items;
  } else {
    return $sorted_menu_items;
  }
}

//*****************************************************************************************************
//Add custom content fields (using Advanced Custom Fields API)
function template_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'theme_acf_group',
		'title' => 'Additional Content',
		'fields' => array (
      array (
				'key' => 'content_1',
				'label' => 'Content 1',
				'name' => 'content_1',
				'type' => 'wysiwyg',
			),
      array (
				'key' => 'content_2',
				'label' => 'Content 2',
				'name' => 'content_2',
				'type' => 'wysiwyg',
			),
      array (
				'key' => 'content_3',
				'label' => 'Content 3',
				'name' => 'content_3',
				'type' => 'wysiwyg',
			)       
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-templates/sub-rc.php',
				)
			),
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-templates/full-width.php',
				)
			),
		),
	));
	
}
add_action('acf/init', 'template_my_acf_add_local_field_groups');



//*****************************************************************************************************
//Redirect location archive/single to contact page, if only 1 location
function smart_redirects(){
    global $wp_query;
  
    if(!$wp_query->is_main_query()) {return;}  
   
    $count_posts = wp_count_posts( 'cpt_location' );
  
    //location page redirects (redirect to contact if only 1 location)    
    if( $count_posts->publish == 1 && $wp_query->query_vars['post_type'] === 'cpt_location' ){
        wp_redirect( '/contact' );
    }
  
  
    //flyer page redirects (redirect to URL if only 1 location)   
    if( $count_posts->publish == 1 && is_page_template( 'page-templates/flyers.php' )  ){
    
        //get post info for first location
        $loop = new WP_Query( array('post_type' => 'cpt_location','meta_key'=> 'location_priority','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 100) );             
        while ( $loop->have_posts() ) : $loop->the_post();
            wp_redirect( 'http://www.pharmasave.com/default/0/flyer.aspx?locale=en&store_code='.get_field('location_store_id') );
        endwhile;
        
    }  
  
    //rxrefill page redirects (redirect to URL if only 1 location)   
    if( $count_posts->publish == 1 && is_page_template( 'page-templates/rxrefill.php' )  ){
    
        //get post info for first location
        $loop = new WP_Query( array('post_type' => 'cpt_location','meta_key'=> 'location_priority','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 100) );             
        while ( $loop->have_posts() ) : $loop->the_post();
      
            $url_custom = get_field('location_rxrefill_url');
      
            if( $url_custom && $url_custom['url'] !== '' )
              wp_redirect( $url_custom['url'] );
            else
              wp_redirect( 'http://www.pharmasave.com/prescriptions/?storeid='.get_field('location_store_id') );
      
        endwhile;
        
    }  
  //email-signup page redirects (redirect to URL if only 1 location)   
    if( $count_posts->publish == 1 && is_page_template( 'page-templates/email-signup.php' )  ){
    
        //get post info for first location
        $loop = new WP_Query( array('post_type' => 'cpt_location','meta_key'=> 'location_priority','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 100) );             
        while ( $loop->have_posts() ) : $loop->the_post();
            wp_redirect( 'http://preferences.pharmasave.com/Preference.aspx?storeid='.get_field('location_store_id') );
        endwhile;
        
    }  
 
  
}

add_action('template_redirect', 'smart_redirects');

 

 function wpb_widgets_init() {
 
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'wpb' ),
        'id' => 'sidebar-1',
        'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'wpb' ),
        'before_widget' => '<aside id="%1$s" class="g-reviews widget %2$s">',
        'after_widget' => '</aside>',
        //'before_title' => '<h3 class="widget-title">',
        //'after_title' => '</h3>',
    ) );
 
    register_sidebar( array(
        'name' =>__( 'Front page sidebar', 'wpb'),
        'id' => 'sidebar-2',
        'description' => __( 'Appears on the static front page template', 'wpb' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
    }
 
add_action( 'widgets_init', 'wpb_widgets_init' );

 
function override_mce_options($initArray) {
	$opts = '*[*]';
	$initArray['valid_elements'] = $opts;
	$initArray['extended_valid_elements'] = $opts;
	return $initArray;
  
}
add_filter('tiny_mce_before_init', 'override_mce_options');



 