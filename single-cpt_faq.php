<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?>  
      <div class="col-lg-9 pb-2">
          <h1><?php echo get_the_title(); ?></h1>
                            
          <ul>                    

            <?php              

              echo '<li>Title (post):'.get_the_title().'</li>';
              echo '<li>Archive Link: <a href="'.get_post_type_archive_link( 'cpt_faq' ).'">'.get_post_type_archive_link( 'cpt_faq' ).'</a></li>';                          
              echo '<li>Question: '.get_field('faq_question').'</li>';
              echo '<li>Answer: '.get_field('faq_answer').'</li>';

            ?>

          </ul>
         
      </div>
       <?php endwhile; endif; ?>
      
			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>