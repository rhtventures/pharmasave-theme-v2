<?php
 
get_header();
 
if ( have_posts() ) :
	?>
 
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white">Search</h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3"> 

            <div class="col-12">
              <h4>Search results for query: "<?php the_search_query(); ?>"</h4>
          
              <?php
              while ( have_posts() ) : the_post(); ?>

                    <article class="post border-bottom pb-3 mb-3">
                      
                            <h3><a href="<?php the_permalink() ?>" class="red"><?php the_title() ?></a></h3>
                            
                            <p>
                        <?php echo get_the_excerpt() ?>
                                <a href="<?php the_permalink() ?>">Read more &raquo</a>
                            </p>
                        </article>

                  <?php endwhile;

            else :
              echo '<div style="padding: 200px 0 100px;text-align:center"><h3>No search results found!</h3></div>';

            endif; ?>
          </div>
      </div><!--/.row-->
    </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 


<?php 	get_footer(); ?>