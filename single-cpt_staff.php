   <?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5 border-bottom">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?><br/><br/> 
      
      <div class="col-lg-9 pb-2">
          <h1 class="mb-2"><?php echo get_field('staff_fname').' '.get_field('staff_lname') ?></h1>
        
          <?php  echo '<h5 class="mb-3">'.get_field('staff_title').'</h5>'; ?>
        
          <?php      
        
            echo '<div class="row mb-4">';
                  echo '<div class="col-sm-4">';

                     $image = get_field('staff_image');
                     echo '<img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';

                  echo '</div>';

                  echo '<div class="col-sm-8">';

                      echo ''.get_field('staff_description').'';
                      echo '<p><a href="/staff"><i class="fas fa-arrow-left"></i> View more Staff</a></p>';

                  echo '</div>';

            echo '</div>';//row
        
           ?>                  
 
      </div>
      <?php endwhile; endif; ?>
      
			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>

 
