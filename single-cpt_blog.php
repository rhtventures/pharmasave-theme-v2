<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
           <h1 class="white"><?php echo get_the_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>
<section class="py-5 page-content">
    <div class="container"> 
         
          <?php      
        
            echo '<div class="row mb-4">';
                  echo '<div class="col-12">';

                      echo '<div class="bg-light p-4 border">';
                        $image = get_field('blog_image');
                        echo '<img class="ir" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                      echo '</div>';

                      echo '<div class="p-5 border">';
                          echo '<h3 class="text-center">'.get_the_title().'</h3>';
                          echo '<div class="date mb-2 text-center">'.get_the_date('F j, Y').'</div>';
                          echo ''.get_field('blog_article').'';
                          
                      echo '</div>';
                  echo '</div>';

            echo '</div>';//row
      
            echo '<div class="row mb-4">';
                echo '<div class="col-12 text-center">';
                    echo '<p><a  class="btn btn-small text-white" href="/blogs"><i class="fas fa-arrow-left"></i> View more Blogs</a>';
               echo '</div>';
                  echo '</div>';
           ?>                  
 
      <?php endwhile;?>

      <?php else : ?>
        There are currently no blog entries.
      <?php endif; ?> 
    
     </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 

<?php 	get_footer(); ?>