<?php get_header(); ?>
<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
           <h1 class="white"><?php post_type_archive_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>

<section class="py-5 page-content">
    <div class="container">
        <div class="row pb-3 border-bottom"> 
          
          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
 
              <?php       
                  
                  echo '<div class="col-lg-6 mb-4 ">';
                      echo '<div class="row bg-light">';
          
                          echo '<div class="col-sm-5">';

                          echo '<a href="'.get_permalink().'" class="text-white">';
                              echo '<div class="home-services relative">';

                                    $image = get_field('service_image');
                                    echo '<img class="img-fluid" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';

                                    echo '<div class="overlay text-center w-100" style="left:0">';
                                        $icon = get_field('service_icon');
                                        echo '<img class="mb-3" src="'.$icon['url'].'" alt="'.$icon['alt'].'" title="'.$icon['title'].'"/>';

                                        
                                    echo '</div>';

                              echo '</div>'; //home-services
                          echo '</a>';

                          echo '</div>';
          
                          echo '<div class="col-sm-7">';
                              echo '<div class="py-4 pl-0 pr-4 h-100">';
                                  echo '<h4 class="my-0">'.get_the_title().'</h4>';
                                  echo wp_trim_words(get_field('service_description'), 34, '... <a href="'.get_permalink().'"> read more</a>' );
                              echo '</div>';
                          echo '</div>';
          
                      echo '</div>';//row
                      
                  echo '</div>';
          
               ?>
            
          <?php endwhile;?>
          </div><!--/.row-->
         
         <div class="row">
          <div class="col-12">
 
              <?php else : ?>
                There are currently no services.
              <?php endif; ?>
          </div>
          <div class="col-12 pt-2">

            <?php echo ''.the_posts_pagination();?>
          </div>
        </div>
 
     </div><!--/.container-->
</section>
 
<section>
     <div class="container">
        <div class="row pb-2 mb-2 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section>  

<?php 	get_footer(); ?>